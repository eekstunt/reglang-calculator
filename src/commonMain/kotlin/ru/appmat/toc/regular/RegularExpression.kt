package ru.appmat.toc.regular

/**
 * Задача:
 * реализовать сам класс как аналог AST и функции из regex-basic
 * Исполнитель: [А-13-17] Костюков Алексей
 */

data class RegularExpression(val value: Component) :
    Regular<RegularExpression> {

    sealed class Component {

        object Lambda : Component() {
            override fun toString(): String {
                return "$LAMBDA"
            }
        }

        data class Letter(val value: Char) : Component() {
            override fun toString(): String {
                return "$value"
            }
        }

        sealed class Quantifier : Component() {

            data class Star(val value: Component) : Quantifier() {
                override fun toString(): String {
                    return if (value is NAryOperator.Concatenation) "($value)*"
                    else "$value*"
                }
            }

            data class Plus(val value: Component) : Quantifier() {
                override fun toString(): String {
                    return if (value is NAryOperator.Concatenation) "($value)+"
                    else "$value+"
                }
            }

            data class Optional(val value: Component) : Quantifier() {
                override fun toString(): String {
                    return if (value is NAryOperator.Concatenation) "($value)?"
                    else "$value?"
                }
            }
        }

        sealed class NAryOperator : Component() {

            data class Alternation(val values: List<Component>) : NAryOperator() {
                init {
                    if (values.size < 2)
                        throw IllegalArgumentException("Alternation with less than 2 values is not allowed")
                }

                override fun toString(): String {
                    return "(${values.joinToString("|")})"
                }
            }

            data class Concatenation(val values: List<Component>) : NAryOperator() {
                init {
                    if (values.isEmpty())
                        throw IllegalArgumentException("Empty Concatenation is not allowed")
                    if (values.size == 1)
                        throw IllegalArgumentException("Redundant Concatenation is not allowed")
                }

                override fun toString(): String {
                    return values.joinToString("")
                }
            }
        }
    }

    private class ModifiedShuntingYard {

        private val output = mutableListOf<Component>()

        // contains (Token.Alternative, position) separated by (Token.BracketOpen, position) (if it exits)
        private val alternativeStack = mutableListOf<Pair<Token, Int>>()

        fun parse(tokens: List<Token>): Component {
            checkValidity(tokens)

            for (token in tokens) {
                when (token) {
                    Token.Lambda -> output.add(Component.Lambda)
                    is Token.Letter -> output.add(Component.Letter(token.value))

                    Token.Star -> {
                        val last = output.last()
                        output.removeAt(output.lastIndex)
                        output.add(Component.Quantifier.Star(last))
                    }
                    Token.Plus -> {
                        val last = output.last()
                        output.removeAt(output.lastIndex)
                        output.add(Component.Quantifier.Plus(last))
                    }

                    Token.Optional -> {
                        val last = output.last()
                        output.removeAt(output.lastIndex)
                        output.add(Component.Quantifier.Optional(last))
                    }

                    Token.Alternation -> alternativeStack.add(Pair(token, output.lastIndex))
                    Token.BracketOpen -> alternativeStack.add(Pair(token, output.lastIndex))
                    Token.BracketClose -> output.add(aggregateAllUntilBrackets())
                }
            }
            return aggregateAllUntilBrackets()
        }

        private fun checkValidity(tokens: List<Token>) {
            val bracketStack = mutableListOf<Token>()
            for (i in tokens.indices) {
                require(tokenPositionIsValid(tokens, i)) { "Illegal position for ${tokens[i]}" }

                when (tokens[i]) {
                    Token.BracketOpen -> bracketStack.add(Token.BracketOpen)
                    Token.BracketClose -> {
                        require(bracketStack.isNotEmpty() && bracketStack.last() is Token.BracketOpen) {
                            "Unbalanced brackets"
                        }
                        bracketStack.removeAt(bracketStack.lastIndex)
                    }
                }
            }
            require(bracketStack.isEmpty()) { "Unbalanced brackets" }
        }

        private fun tokenPositionIsValid(tokens: List<Token>, i: Int) = when {
            tokens[i] in setOf(Token.Star, Token.Plus, Token.Optional) ->
                i != 0 && tokens[i - 1] !in setOf(Token.Alternation, Token.BracketOpen)
            tokens[i] is Token.Alternation ->
                i !in setOf(0, tokens.lastIndex) && tokens[i - 1] !in setOf(Token.BracketOpen, Token.Alternation) &&
                        (tokens[i + 1] in setOf(Token.BracketOpen, Token.Lambda) || tokens[i + 1] is Token.Letter)

            else -> true
        }

        private fun aggregateAllUntilBrackets(): Component {
            val alternativeConcatenations = mutableListOf(mutableListOf<Component>())
            for (i in output.lastIndex downTo 0) {
                if (alternativeStack.size > 0 && alternativeStack.last() == Pair(Token.Alternation, i)) {
                    alternativeConcatenations.add(0, mutableListOf())
                    alternativeStack.removeAt(alternativeStack.lastIndex)
                }
                if (alternativeStack.size > 0 && alternativeStack.last() == Pair(Token.BracketOpen, i)) {
                    alternativeStack.removeAt(alternativeStack.lastIndex)
                    break
                }
                alternativeConcatenations[0].add(0, output.last())
                output.removeAt(output.lastIndex)
            }

            return aggregateAlternativeConcatenation(alternativeConcatenations)
        }

        private fun aggregateAlternativeConcatenation(alternativeConcatenations: MutableList<MutableList<Component>>) =
            when (alternativeConcatenations.size) {
                1 -> when (alternativeConcatenations.last().size) {
                    0 -> Component.Lambda
                    1 -> alternativeConcatenations.last().last()
                    else -> Component.NAryOperator.Concatenation(alternativeConcatenations.last())
                }
                else -> Component.NAryOperator.Alternation(
                    alternativeConcatenations.flatMap {
                        val last = it.last()
                        when {
                            it.size == 1 && last is Component.NAryOperator.Alternation -> last.values
                            it.size == 1 -> it
                            else -> listOf(Component.NAryOperator.Concatenation(it))
                        }
                    }
                )
            }
    }

    private sealed class Token(val description: String) {
        object Lambda : Token("Lambda")
        class Letter(val value: Char) : Token("Letter $value")
        object Star : Token("Quantifier *")
        object Plus : Token("Quantifier +")
        object Optional : Token("Quantifier ?")
        object Alternation : Token("Alternation |")
        object BracketOpen : Token("Open Bracket [")
        object BracketClose : Token("Close Bracket ]")
    }

    companion object {
        fun of(expr: String): RegularExpression {
            if (expr.isEmpty())
                return RegularExpression(Component.Lambda)

            val tokens = mutableListOf<Token>()

            for (char in expr) {
                when (char) {
                    '*' -> tokens.add(Token.Star)
                    '+' -> tokens.add(Token.Plus)
                    '?' -> tokens.add(Token.Optional)
                    '|' -> tokens.add(Token.Alternation)
                    '(' -> tokens.add(Token.BracketOpen)
                    ')' -> tokens.add(Token.BracketClose)
                    LAMBDA -> tokens.add(Token.Lambda)
                    else -> tokens.add(Token.Letter(char))
                }
            }

            return RegularExpression(ModifiedShuntingYard().parse(tokens))
        }
    }

    override fun toString(): String {
        return if (value is Component.NAryOperator.Alternation)
            return value.values.joinToString("|")
        else "$value"
    }

    enum class RegExpToNFAConversionAlgorithm {
        Thompson,
        Glushkov
    }

    //region regex-basic
    override fun concatenate(other: RegularExpression): RegularExpression {
        if (this.value is Component.Lambda)
            return other
        if (other.value is Component.Lambda)
            return this

        val between = mutableListOf<Component>()
        val firstValues: List<Component>

        when (this.value) {
            is Component.NAryOperator.Alternation -> {
                firstValues = this.value.values.take(this.value.values.size - 1)
                between.add(this.value.values.last())
            }
            is Component.NAryOperator.Concatenation -> {
                firstValues = this.value.values.take(this.value.values.size - 1)
                between.add(this.value.values.last())
            }
            else -> {
                firstValues = listOf()
                between.add(this.value)
            }
        }

        val secondValues: List<Component>
        when (other.value) {
            is Component.NAryOperator.Alternation -> {
                secondValues = other.value.values.takeLast(other.value.values.size - 1)
                between.add(other.value.values.first())
            }
            is Component.NAryOperator.Concatenation -> {
                secondValues = other.value.values.takeLast(other.value.values.size - 1)
                between.add(other.value.values.first())
            }
            else -> {
                secondValues = listOf()
                between.add(other.value)
            }
        }

        val values = mutableListOf<Component>()
        val firstIsAlternation = this.value is Component.NAryOperator.Alternation
        val secondIsAlternation = other.value is Component.NAryOperator.Alternation

        return if (firstIsAlternation && secondIsAlternation) {
            values.addAll(firstValues)
            values.add(Component.NAryOperator.Concatenation(between))
            values.addAll(secondValues)
            RegularExpression(Component.NAryOperator.Alternation(values))

        } else if (firstIsAlternation) {
            values.addAll(firstValues)
            values.add(Component.NAryOperator.Concatenation(between + secondValues))
            RegularExpression(Component.NAryOperator.Alternation(values))

        } else if (secondIsAlternation) {
            values.add(Component.NAryOperator.Concatenation(firstValues + between))
            values.addAll(secondValues)
            RegularExpression(Component.NAryOperator.Alternation(values))

        } else {
            values.addAll(firstValues)
            values.addAll(between)
            values.addAll(secondValues)
            RegularExpression(Component.NAryOperator.Concatenation(values))
        }
    }

    override fun union(other: RegularExpression): RegularExpression {
        if (this.value is Component.Lambda && other.value is Component.Lambda)
            return this

        val firstValues: List<Component> = when (this.value) {
            is Component.NAryOperator.Alternation -> this.value.values
            else -> listOf(this.value)
        }

        val secondValues: List<Component> = when (other.value) {
            is Component.NAryOperator.Alternation -> other.value.values
            else -> listOf(other.value)
        }

        return RegularExpression(Component.NAryOperator.Alternation(firstValues + secondValues))
    }

    override fun plusClosure(): RegularExpression {
        if (this.value is Component.Lambda)
            return this

        return RegularExpression(Component.Quantifier.Plus(this.value))
    }

    override fun starClosure(): RegularExpression {
        if (this.value is Component.Lambda)
            return this

        return RegularExpression(Component.Quantifier.Star(this.value))
    }
    //endregion regex-basic

    override fun intersect(other: RegularExpression): RegularExpression {
        TODO("Not yet implemented")
    }

    /**
     * Задача:
     * дополнение к языку и его реверс в виде регулярки
     */
    //region regex-complement
    override fun complement(): RegularExpression {
        TODO("Not yet implemented")
    }

    override fun reversed(): RegularExpression {
        TODO("Not yet implemented")
    }
    //endregion

    override fun check(word: Word): Boolean {
        return this.toNFA().check(word)
    }

    fun toNFA(
        algorithm: RegExpToNFAConversionAlgorithm = RegExpToNFAConversionAlgorithm.Thompson
    ): FiniteAutomaton.NFA {
        return when (algorithm) {
            RegExpToNFAConversionAlgorithm.Thompson -> thompsonConversion()
            RegExpToNFAConversionAlgorithm.Glushkov -> glushkovConversion()
        }
    }

    /**
     * Задача:
     * Приведение к правой регулярной грамматике
     * Исполнитель: [А-13-17] Бернадская Алина
     */
    fun toRightGrammar(): RegularGrammar.RightRegularGrammar {
        TODO("Not yet implemented")
    }


    /**
     * Задача:
     * Приведение к левой регулярной грамматике
     */
    fun toLeftGrammar(): RegularGrammar.LeftRegularGrammar {
        TODO("Not yet implemented")
    }

    fun toDFA() = toNFA().toDFA()

    /**
     * Задача:
     * алгоритм Глушкова @see https://en.wikipedia.org/wiki/Glushkov%27s_construction_algorithm
     * Исполнитель: [А-05-17] Крылов Кирилл
     */
    private fun glushkovConversion(): FiniteAutomaton.NFA {
        TODO("Not yet implemented")
    }

    /**
     * Задача:
     * алгоритм Томпсона @see https://en.wikipedia.org/wiki/Thompson%27s_construction
     * Исполнитель: [А-05-17] Бабенов Артем
     */
    private fun thompsonConversion(): FiniteAutomaton.NFA {
        TODO("Not yet implemented")
    }
}

