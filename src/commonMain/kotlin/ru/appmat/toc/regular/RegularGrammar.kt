package ru.appmat.toc.regular

enum class Orientation {
    Right, Left
}

data class NonTerminalSymbol(val letter: Char, val index: UInt = 0u) {
    override fun toString(): String {
        return "${letter.toUpperCase()}$index"
    }
}

sealed class RegularGrammarRule {
    object EmptyRule : RegularGrammarRule() {
        override fun toString(): String = LAMBDA.toString()
    }

    data class LetterRule(val letter: Char) : RegularGrammarRule() {
        override fun toString() = "$letter"
    }

    sealed class NonTerminalRule(val orientation: Orientation) : RegularGrammarRule() {

        data class RightRule(val letter: Char, val nonTerminal: NonTerminalSymbol) :
            NonTerminalRule(Orientation.Right) {
            override fun toString(): String = "$letter$nonTerminal"
        }

        data class LeftRule(val letter: Char, val nonTerminal: NonTerminalSymbol) :
            NonTerminalRule(Orientation.Left) {
            override fun toString(): String = "$nonTerminal$letter"
        }
    }
}


sealed class RegularGrammar<T : Regular<T>>(
    open val startVariable: NonTerminalSymbol,
    open val rules: Map<NonTerminalSymbol, Set<RegularGrammarRule>>,
    open val orientation: Orientation
) : Regular<T> {

    fun isValid() = rules.values.flatten().all {
        when (it) {
            is RegularGrammarRule.NonTerminalRule -> it.orientation == orientation
            else -> true
        }
    }

    abstract fun toLambdaFree(): T

    abstract fun withRule(rule: RegularGrammarRule): T

    abstract fun renameNonTerminals(letter: Letter): T

    data class LeftRegularGrammar(
        override val startVariable: NonTerminalSymbol,
        override val rules: Map<NonTerminalSymbol, Set<RegularGrammarRule>>
    ) : RegularGrammar<LeftRegularGrammar>(startVariable, rules, Orientation.Left) {

        init {
            require(isValid())
        }

        /**
         * Задача:
         * звезда Клини
         * Исполнитель: [A-05-17] Попов Владимир
         */
        //region left-grammar-closure
        override fun starClosure(): LeftRegularGrammar {
            TODO("Not yet implemented")
        }

        override fun plusClosure(): LeftRegularGrammar {
            TODO("Not yet implemented")
        }
        //endregion


        /**
         * Задача:
         * конкатенация
         * Исполнитель [А-05-17] Окованцев Павел
         */
        override fun concatenate(other: LeftRegularGrammar): LeftRegularGrammar {
            /* Раскомментить как только будет реализован метод toLambdaFree
            var wasLambda = false
            for (ruleSet in this.rules.values)
                if (RegularGrammarRule.EmptyRule in ruleSet) {
                    wasLambda = true
                    break
                }
            this.toLambdaFree()
            */

            val renamedThis = this.renameNonTerminals('A')
            val renamedOther = other.renameNonTerminals('B')

            val resRules =
                renamedThis.rules.map { (nonTerminal, ruleSet) ->
                    nonTerminal to ruleSet.map { rule ->
                        when (rule) {
                            is RegularGrammarRule.LetterRule -> RegularGrammarRule.NonTerminalRule.LeftRule(
                                rule.letter,
                                renamedOther.startVariable
                            )
                            else -> rule
                        }
                    }.toSet()
                }.union(renamedOther.rules.toList()).toMap()

            /* Раскомментить как только будет реализован метод toLambdaFree
            if (wasLambda) {
                val startSet = mutableSetOf<RegularGrammarRule>()
                startSet.addAll(resMap[renamedThis.startVariable] as Set<RegularGrammarRule>)
                startSet.add(RegularGrammarRule.EmptyRule)
                resMap[renamedThis.startVariable] = startSet
            }
            */
            return LeftRegularGrammar(renamedThis.startVariable, resRules)
        }

        override fun renameNonTerminals(letter: Letter): LeftRegularGrammar {
            // old nonTerminalSymbol -> new nonTerminalSymbol
            val nonTerminalsMap =
                this.rules.keys.mapIndexed { index, old -> Pair(old, NonTerminalSymbol(letter, index.toUInt())) }
                    .toMap()

            val newRules = this.rules.map { (nonTerminal, ruleSet) ->
                nonTerminalsMap[nonTerminal]!! to ruleSet.map { rule ->
                    if (rule is RegularGrammarRule.NonTerminalRule.LeftRule)
                        rule.copy(nonTerminal = nonTerminalsMap[rule.nonTerminal]!!)
                    else rule
                }.toSet()
            }.toMap()

            return LeftRegularGrammar(nonTerminalsMap[this.startVariable]!!, newRules)
        }

        override fun intersect(other: LeftRegularGrammar): LeftRegularGrammar {
            TODO("Not yet implemented")
        }


        override fun complement(): LeftRegularGrammar {
            TODO("Not yet implemented")
        }

        /**
         * Задача:
         * объединение
         * Исполнитель [А-13-17] Бернадская Алина
         */
        override fun union(other: LeftRegularGrammar): LeftRegularGrammar {
            TODO("Not yet implemented")
        }

        /**
         * Задача:
         * грамматика для реверса языка
         */
        override fun reversed(): LeftRegularGrammar {
            TODO("Not yet implemented")
        }

        override fun withRule(rule: RegularGrammarRule): LeftRegularGrammar {
            TODO("Not yet implemented")
        }

        /**
         * Задача:
         * КЯК
         * Исполнитель: [А-05-17] Хижин Вадим
         */
        override fun check(word: Word): Boolean {
            TODO("Not yet implemented")
        }

        /**
         * Задача:
         * Преобразование к НКА
         */
        fun toNFA(): FiniteAutomaton.NFA {
            TODO("Not yet implemented")
        }

        /**
         * Задача:
         * Преобразование к правой грамматике
         */
        fun toRight(): RightRegularGrammar {
            TODO("Not yet implemented")
        }

        /**
         * Задача:
         * Избавиться от всех lambda, кроме S -> lambda
         */
        override fun toLambdaFree(): LeftRegularGrammar {
            TODO("Not yet implemented")
        }

    }

    data class RightRegularGrammar(
        override val startVariable: NonTerminalSymbol,
        override val rules: Map<NonTerminalSymbol, Set<RegularGrammarRule>>
    ) : RegularGrammar<RightRegularGrammar>(
        startVariable,
        rules,
        Orientation.Right
    ) {
        init {
            require(isValid())
        }

        /**
         * Задача:
         * звезда Клини
         * Исполнитель: [А-05-17] Горшков Виктор
         */
//region right-grammar-closure
        override fun starClosure(): RightRegularGrammar {
            TODO("Not yet implemented")
        }

        override fun plusClosure(): RightRegularGrammar {
            TODO("Not yet implemented")
        }
//endregion

        /**
         * Задача:
         * конкатенация
         * Исполнитель: [А-05-17] Коротаев Евгений
         */
        override fun concatenate(other: RightRegularGrammar): RightRegularGrammar {
            TODO("Not yet implemented")
        }

        override fun intersect(other: RightRegularGrammar): RightRegularGrammar {
            TODO("Not yet implemented")
        }

        override fun complement(): RightRegularGrammar {
            TODO("Not yet implemented")
        }

        /**
         * Задача:
         * объединение
         * Исполнитель: [А-13-17] Алиева Ирада
         */
        override infix fun union(other: RightRegularGrammar): RightRegularGrammar {
            val renamedThis = this.renameNonTerminals('A')
            val renamedOther = other.renameNonTerminals('B')
            val startVariable = NonTerminalSymbol('S', 0u)

            val unionRules = mutableMapOf<NonTerminalSymbol, Set<RegularGrammarRule>>()
            unionRules[startVariable] = renamedThis.rules[renamedThis.startVariable]!!.union(
                renamedOther.rules[renamedOther.startVariable]!!
            )

            unionRules.putAll(renamedThis.rules)
            unionRules.putAll(renamedOther.rules)

            return RightRegularGrammar(startVariable, unionRules)
        }

        /**
         * Задача:
         * грамматика для реверса языка
         */
        override fun reversed(): RightRegularGrammar {
            TODO("Not yet implemented")
        }

        override fun withRule(rule: RegularGrammarRule): RightRegularGrammar {
            TODO("Not yet implemented")
        }

        /**
         * Задача:
         * КЯК
         * Исполнитель: [A-13-17] Сидоров Кирилл
         */
        override fun check(word: Word): Boolean {
            TODO("Not yet implemented")
        }


        /**
         * Задача:
         * Преобразование к НКА
         */
        fun toNFA(): FiniteAutomaton.NFA {
            TODO("Not yet implemented")
        }

        /**
         * Задача:
         * Преобразование к левой грамматике
         * Исполнитель: [A-05-17] Курбанов Амирбег
        :*/
        fun toLeft(): LeftRegularGrammar {
            TODO("Not yet implemented")
        }

        fun toRegExp(): RegularExpression = toNFA().toRegExp()
        fun toDFA(): FiniteAutomaton.DFA = toNFA().toDFA()

        /**
         * Задача:
         * Избавиться от всех lambda, кроме S -> lambda
         * Исполнитель: [A-05-17] Никишин Даниил
         */
        override fun toLambdaFree(): RightRegularGrammar {
            TODO("Not yet implemented")
        }

        override fun renameNonTerminals(letter: Letter): RightRegularGrammar {
            // old nonTerminalSymbol -> new nonTerminalSymbol
            val nonTerminalsMap =
                this.rules.keys.mapIndexed { index, old -> Pair(old, NonTerminalSymbol(letter, index.toUInt())) }
                    .toMap()

            val newRules = this.rules.map { (nonTerminal, ruleSet) ->
                nonTerminalsMap[nonTerminal]!! to ruleSet.map { rule ->
                    if (rule is RegularGrammarRule.NonTerminalRule.RightRule)
                        rule.copy(nonTerminal = nonTerminalsMap[rule.nonTerminal]!!)
                    else rule
                }.toSet()
            }.toMap()

            return RightRegularGrammar(nonTerminalsMap[this.startVariable]!!, newRules)
        }
    }

}
