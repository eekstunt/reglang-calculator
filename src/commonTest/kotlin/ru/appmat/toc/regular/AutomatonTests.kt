package ru.appmat.toc.regular

import kotlin.test.*

class AutomatonTests {

    // dfa-basic tests
    @Test
    fun `DFA initialization`() {
        //Количество букв b по модулю 3 было равно 2.

        val a: CharAlphabet = setOf<Letter>('a', 'b')

        val transitionsTable: List<Map<Char, Int>> = listOf(
            mapOf(
                'a' to 0,
                'b' to 1
            ),
            mapOf(
                'a' to 1,
                'b' to 2
            ),
            mapOf(
                'a' to 2,
                'b' to 0
            )
        )
        val stateCount: Int = transitionsTable.count()

        val startState: Int = 0
        val terminalStates: Set<Int> = setOf(2)
        val automata1 = FiniteAutomaton.DFA(
            stateCount,
            a,
            transitionsTable,
            startState,
            terminalStates
        )

        assertTrue(automata1.check("baaba"))
        assertTrue(automata1.check("bbbbb"))
        assertTrue(!automata1.check("bbddebbb"))
    }

    @Test
    fun `DFA copy`() {
        //Количество букв b по модулю 3 было равно 2.
        val a: CharAlphabet = setOf<Letter>('a', 'b')

        val transitionsTable: List<Map<Char, Int>> = listOf(
            mapOf(
                'a' to 0,
                'b' to 1
            ),
            mapOf(
                'a' to 1,
                'b' to 2
            ),
            mapOf(
                'a' to 2,
                'b' to 0
            )
        )
        val stateCount: Int = transitionsTable.count()
        val startState: Int = 0
        val terminalStates: Set<Int> = setOf(2)
        val automata1 = FiniteAutomaton.DFA(
            stateCount,
            a,
            transitionsTable,
            startState,
            terminalStates
        )
        val automata2 = automata1.copy()
        assertEquals(automata1, automata2)
    }

    @Test
    fun `Transitions Table does not have any transition`() {
        //Количество букв b по модулю 3 было равно 2.
        val a: CharAlphabet = setOf<Letter>('a', 'b')

        val transitionsTable: List<Map<Char, Int>> = listOf(
            mapOf(
                'a' to 0,
                'b' to 1
            ),
            mapOf(
                'a' to 1
            ),
            mapOf(
                'a' to 2,
                'b' to 0
            )
        )
        val stateCount: Int = transitionsTable.count()
        val startState: Int = 0
        val terminalStates: Set<Int> = setOf(2)

        val exception = assertFailsWith<IllegalArgumentException> {
            val automata1 = FiniteAutomaton.DFA(
                stateCount,
                a,
                transitionsTable,
                startState,
                terminalStates
            )
        }

    }

    @Test
    fun `Transitions Table has transition by symbol that are not in alphabet`() {
        //Количество букв b по модулю 3 было равно 2.
        val a: CharAlphabet = setOf<Letter>('a', 'b')

        val transitionsTable: List<Map<Char, Int>> = listOf(
            mapOf(
                'a' to 0,
                'b' to 1
            ),
            mapOf(
                'a' to 1,
                'b' to 2,
                'c' to 2
            ),
            mapOf(
                'a' to 2,
                'b' to 0
            )
        )
        val stateCount: Int = transitionsTable.count()
        val startState: Int = 0
        val terminalStates: Set<Int> = setOf(2)

        val exception = assertFailsWith<IllegalArgumentException> {
            val automata1 = FiniteAutomaton.DFA(
                stateCount,
                a,
                transitionsTable,
                startState,
                terminalStates
            )
        }

    }

    @Test
    fun `Transitions Table has transitions to a nonexistent state`() {
        //Количество букв b по модулю 3 было равно 2.
        val a: CharAlphabet = setOf<Letter>('a', 'b')

        val transitionsTable: List<Map<Char, Int>> = listOf(
            mapOf(
                'a' to 0,
                'b' to 1
            ),
            mapOf(
                'a' to 1,
                'b' to 4
            ),
            mapOf(
                'a' to 2,
                'b' to 0
            )
        )
        val stateCount: Int = transitionsTable.count()
        val startState: Int = 0
        val terminalStates: Set<Int> = setOf(2)

        val exception = assertFailsWith<IllegalArgumentException> {
            val automata1 = FiniteAutomaton.DFA(
                stateCount,
                a,
                transitionsTable,
                startState,
                terminalStates
            )
        }

    }

    @Test
    fun `withAlphabet method does not contain DFA alphabet`() {
        //Количество букв b по модулю 3 было равно 2.
        val a: CharAlphabet = setOf<Letter>('a', 'b')

        val transitionsTable: List<Map<Char, Int>> = listOf(
            mapOf(
                'a' to 0,
                'b' to 1
            ),
            mapOf(
                'a' to 1,
                'b' to 2
            ),
            mapOf(
                'a' to 2,
                'b' to 0
            )
        )
        val stateCount: Int = transitionsTable.count()
        val startState: Int = 0
        val terminalStates: Set<Int> = setOf(2)

        val automata1 = FiniteAutomaton.DFA(
            stateCount,
            a,
            transitionsTable,
            startState,
            terminalStates
        )
        val a2 = setOf<Letter>('a', 'c', 'd')
        assertFailsWith<IllegalArgumentException> {
            val automata2 = automata1.withAlphabet(a2)
        }
        assertTrue(automata1.check("baaba"))
        assertTrue(automata1.check("bbbbb"))
        assertTrue(!automata1.check("bbddebbb"))

    }

    @Test
    fun `withAlphabet method equal DFA alphabet`() {
        //Количество букв b по модулю 3 было равно 2.
        val a: CharAlphabet = setOf<Letter>('a', 'b')

        val transitionsTable: List<Map<Char, Int>> = listOf(
            mapOf(
                'a' to 0,
                'b' to 1
            ),
            mapOf(
                'a' to 1,
                'b' to 2
            ),
            mapOf(
                'a' to 2,
                'b' to 0
            )
        )
        val stateCount: Int = transitionsTable.count()
        val startState: Int = 0
        val terminalStates: Set<Int> = setOf(2)

        val automata1 = FiniteAutomaton.DFA(
            stateCount,
            a,
            transitionsTable,
            startState,
            terminalStates
        )
        val a2 = setOf<Letter>('a', 'b')
        val automata2 = automata1.withAlphabet(a2)

        assertEquals(automata1, automata2)
        assertTrue(automata1.check("baaba"))
        assertTrue(automata1.check("bbbbb"))
        assertTrue(!automata1.check("bbddebbb"))

    }

    @Test
    fun `withAlphabet method`() {
        //Количество букв b по модулю 3 было равно 2.
        val a: CharAlphabet = setOf<Letter>('a', 'b')

        val transitionsTable: List<Map<Char, Int>> = listOf(
            mapOf(
                'a' to 0,
                'b' to 1
            ),
            mapOf(
                'a' to 1,
                'b' to 2
            ),
            mapOf(
                'a' to 2,
                'b' to 0
            )
        )
        val stateCount: Int = transitionsTable.count()
        val startState: Int = 0
        val terminalStates: Set<Int> = setOf(2)

        val automata1 = FiniteAutomaton.DFA(
            stateCount,
            a,
            transitionsTable,
            startState,
            terminalStates
        )
        val a2 = setOf<Letter>('a', 'b', 'c')
        val automata2 = automata1.withAlphabet(a2)

        assertNotEquals(automata1, automata2)
        assertEquals(automata1.stateCount + 1, automata2.stateCount)

        assertTrue(automata1.check("baaba"))
        assertTrue(automata1.check("bbbbb"))
        assertTrue(!automata1.check("bbddebbb"))

        assertTrue(automata2.check("baaba"))
        assertTrue(automata2.check("bbbbb"))
        assertTrue(!automata2.check("bbddebbb"))

    }

    @Test
    fun `complement method`() {
        //Количество букв b по модулю 3 было равно 2.
        val a: CharAlphabet = setOf<Letter>('a', 'b')

        val transitionsTable: List<Map<Char, Int>> = listOf(
            mapOf(
                'a' to 0,
                'b' to 1
            ),
            mapOf(
                'a' to 1,
                'b' to 2
            ),
            mapOf(
                'a' to 2,
                'b' to 0
            )
        )
        val stateCount: Int = transitionsTable.count()
        val startState: Int = 0
        val terminalStates: Set<Int> = setOf(2)

        val automata1 = FiniteAutomaton.DFA(
            stateCount,
            a,
            transitionsTable,
            startState,
            terminalStates
        )

        val automata2 = automata1.complement()

        assertNotEquals(automata1, automata2)
        assertEquals(automata1.stateCount, automata2.stateCount)
        assertEquals(automata2.terminalStates, setOf(0, 1))

        assertTrue(automata1.check("baaba"))
        assertTrue(automata1.check("bbbbb"))
        assertTrue(!automata1.check("bbddebbb"))

        assertTrue(!automata2.check("baaba"))
        assertTrue(!automata2.check("bbbbb"))
        assertTrue(automata2.check("bbbba"))
        assertTrue(!automata2.check("bbddebbb"))

    }
    //endregion dfa-basic tests
    
	
	//dfa-deleteUnreachableStates tests
    @Test
    fun `deleteUnreachableStates first test`() {
        //Количество букв b по модулю 3 было равно 2.

        val a: CharAlphabet = setOf<Letter>('a', 'b')


        val transitionsTable: List<Map<Char, Int>> = listOf(

                mapOf(
                        'a' to 0,
                        'b' to 1
                ),
                mapOf(
                        'a' to 1,
                        'b' to 3
                ),
                mapOf(
                        'a' to 0,
                        'b' to 1
                ),
                mapOf(
                        'a' to 3,
                        'b' to 4
                ),
                mapOf(
                        'a' to 4,
                        'b' to 1
                ),
                mapOf(
                        'a' to 0,
                        'b' to 1
                ),
                mapOf(
                        'a' to 0,
                        'b' to 1
                )
        )

        val stateCount: Int = transitionsTable.count()

        val startState: Int = 1
        val terminalStates: Set<Int> = setOf(4)
        val automata1 = FiniteAutomaton.DFA(
                stateCount,
                a,
                transitionsTable,
                startState,
                terminalStates
        )


        val transitionsTable2: List<Map<Char, Int>> = listOf(
                mapOf(
                        'a' to 0,
                        'b' to 1
                ),
                mapOf(
                        'a' to 1,
                        'b' to 2
                ),
                mapOf(
                        'a' to 2,
                        'b' to 0
                )
        )

        val stateCount2: Int = transitionsTable2.count()

        val startState2: Int = 0
        val terminalStates2: Set<Int> = setOf(2)

        val automata2 = FiniteAutomaton.DFA(
                stateCount2,
                a,
                transitionsTable2,
                startState2,
                terminalStates2
        )


        val automata1DeletedUnreachable = automata1.deleteUnreachableStates()



        assertEquals(automata1DeletedUnreachable, automata2)

    }

    @Test
    fun `deleteUnreachableStates  second test`() {
        //автомат принимает а или б или пустое слово

        val a: CharAlphabet = setOf<Letter>('a', 'b', 'c')


        val transitionsTable: List<Map<Char, Int>> = listOf(
                mapOf(
                        'a' to 2,
                        'b' to 2,
                        'c' to 0
                ),
                mapOf(
                        'a' to 0,
                        'b' to 1,
                        'c' to 2
                ),
                mapOf(
                        'a' to 4,
                        'b' to 4,
                        'c' to 2
                ),
                mapOf(
                        'a' to 0,
                        'b' to 1,
                        'c' to 2
                ),
                mapOf(
                        'a' to 4,
                        'b' to 4,
                        'c' to 4
                ),
                mapOf(
                        'a' to 0,
                        'b' to 1,
                        'c' to 2
                ),
                mapOf(
                        'a' to 2,
                        'b' to 2,
                        'c' to 2
                )
        )

        val stateCount: Int = transitionsTable.count()

        val startState: Int = 0
        val terminalStates: Set<Int> = setOf(0, 2)
        val automata1 = FiniteAutomaton.DFA(
                stateCount,
                a,
                transitionsTable,
                startState,
                terminalStates
        )
        val automata1DeletedUnreachable = automata1.deleteUnreachableStates()

        val transitionsTable2: List<Map<Char, Int>> = listOf(
                mapOf(
                        'a' to 1,
                        'b' to 1,
                        'c' to 0
                ),
                mapOf(
                        'a' to 2,
                        'b' to 2,
                        'c' to 1
                ),
                mapOf(
                        'a' to 2,
                        'b' to 2,
                        'c' to 2
                )
        )
        val stateCount2: Int = transitionsTable2.count()
        val startState2: Int = 0
        val terminalStates2: Set<Int> = setOf(0, 1)
        val automata2 = FiniteAutomaton.DFA(
                stateCount2,
                a,
                transitionsTable2,
                startState2,
                terminalStates2
        )

        assertEquals(automata1DeletedUnreachable, automata2)

    }
    // endregion dfa-deleteUnreachableStates tests



    //dfa-intersection tests
    private fun createFirstDFA(): FiniteAutomaton.DFA {
        val alphabet1: CharAlphabet = setOf('0', '1')
        val startState1 = 0
        val terminalStates1: Set<Int> = setOf(1)
        val transitionsTable1: List<Map<Char, Int>> = listOf(
            mapOf(
                '0' to 0,
                '1' to 1
            ),
            mapOf(
                '0' to 1,
                '1' to 1
            )
        )
        val stateCount1 = transitionsTable1.count()
        return FiniteAutomaton.DFA(stateCount1, alphabet1, transitionsTable1, startState1, terminalStates1)
    }

    private fun createSecondDFA(): FiniteAutomaton.DFA {
        val alphabet2: CharAlphabet = setOf('0', '1')
        val startState2 = 0
        val terminalStates2: Set<Int> = setOf(2, 3)
        val transitionsTable2: List<Map<Char, Int>> = listOf(
            mapOf(
                '0' to 1,
                '1' to 0
            ),
            mapOf(
                '0' to 1,
                '1' to 2
            ),
            mapOf(
                '0' to 2,
                '1' to 2
            ),
            mapOf(
                '0' to 3,
                '1' to 3
            )
        )
        val stateCount2 = transitionsTable2.count()
        return FiniteAutomaton.DFA(stateCount2, alphabet2, transitionsTable2, startState2, terminalStates2)
    }

    /**
     * Пример с wiki: @see https://neerc.ifmo.ru/wiki/index.php?title=Прямое_произведение_ДКА
     */
    @Test
    fun `Intersection test`() {
        val dfa1 = createFirstDFA()
        val dfa2 = createSecondDFA()

        // expected intersected dfa
        val alphabet3: CharAlphabet = setOf('0', '1')
        val startState3 = 0
        val terminalStates3: Set<Int> = setOf(6, 7)
        val transitionsTable3: List<Map<Char, Int>> = listOf(
            mapOf(
                '0' to 1,
                '1' to 4
            ),
            mapOf(
                '0' to 1,
                '1' to 6
            ),
            mapOf(
                '0' to 2,
                '1' to 6
            ),
            mapOf(
                '0' to 3,
                '1' to 7
            ),
            mapOf(
                '0' to 5,
                '1' to 4
            ),
            mapOf(
                '0' to 5,
                '1' to 6
            ),
            mapOf(
                '0' to 6,
                '1' to 6
            ),
            mapOf(
                '0' to 7,
                '1' to 7
            )
        )
        val stateCount3 = transitionsTable3.count()
        val expectedIntersectedDFA =
            FiniteAutomaton.DFA(stateCount3, alphabet3, transitionsTable3, startState3, terminalStates3)

        val intersectedDFA = dfa1.intersect(dfa2)
        val intersectedDFA2 = dfa2.intersect(dfa1)
        assertEquals(expectedIntersectedDFA, intersectedDFA)
        assertEquals(expectedIntersectedDFA, intersectedDFA2)

        assertFalse(intersectedDFA.check("1"))
        assertFalse(intersectedDFA.check("1111"))
        assertFalse(intersectedDFA.check("0000"))
        assertTrue(intersectedDFA.check("01"))
        assertTrue(intersectedDFA.check("001"))
        assertTrue(intersectedDFA.check("0011"))
    }

    /**
     * Пример объединения с wiki: @see https://neerc.ifmo.ru/wiki/index.php?title=Прямое_произведение_ДКА
     */
    @Test
    fun `Union test`() {
        val dfa1 = createFirstDFA()
        val dfa2 = createSecondDFA()

        // expected intersected dfa
        val alphabet3: CharAlphabet = setOf('0', '1')
        val startState3 = 0
        val terminalStates3: Set<Int> = setOf(2, 3, 4, 5, 6, 7)
        val transitionsTable3: List<Map<Char, Int>> = listOf(
            mapOf(
                '0' to 1,
                '1' to 4
            ),
            mapOf(
                '0' to 1,
                '1' to 6
            ),
            mapOf(
                '0' to 2,
                '1' to 6
            ),
            mapOf(
                '0' to 3,
                '1' to 7
            ),
            mapOf(
                '0' to 5,
                '1' to 4
            ),
            mapOf(
                '0' to 5,
                '1' to 6
            ),
            mapOf(
                '0' to 6,
                '1' to 6
            ),
            mapOf(
                '0' to 7,
                '1' to 7
            )
        )
        val stateCount3 = transitionsTable3.count()
        val expectedUnitedDFA =
            FiniteAutomaton.DFA(stateCount3, alphabet3, transitionsTable3, startState3, terminalStates3)


        val unitedDFA = dfa1.union(dfa2)
        val unitedDFA2 = dfa2.union(dfa1)
        assertEquals(expectedUnitedDFA, unitedDFA)
        assertEquals(expectedUnitedDFA, unitedDFA2)

        assertTrue(unitedDFA.check("1"))
        assertTrue(unitedDFA.check("1111"))
        assertTrue(unitedDFA.check("01"))
        assertTrue(unitedDFA.check("001"))
        assertTrue(unitedDFA.check("0011"))
        assertFalse(unitedDFA.check("0"))
        assertFalse(unitedDFA.check("00"))
        assertFalse(unitedDFA.check("000"))
    }
    //endregion dfa-intersection tests
}