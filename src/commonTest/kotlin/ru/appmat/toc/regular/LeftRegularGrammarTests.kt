package ru.appmat.toc.regular

import kotlin.test.*

typealias LeftRegularGrammar = RegularGrammar.LeftRegularGrammar
typealias LeftRule = RegularGrammarRule.NonTerminalRule.LeftRule
typealias LetterRule = RegularGrammarRule.LetterRule
typealias EmptyRule = RegularGrammarRule.EmptyRule
class LeftRegularGrammarTests {
    @Test
    fun renameNonTerminalsTests() {
        var grammarA = LeftRegularGrammar(
            NonTerminalSymbol('S', 0u),
            mapOf(
                NonTerminalSymbol('S', 0u) to setOf(
                    LeftRule('a', NonTerminalSymbol('S', 0u)),
                    LeftRule('b', NonTerminalSymbol('B', 0u))
                ),
                NonTerminalSymbol('B', 0u) to setOf(
                    LetterRule('b'),
                    LetterRule('c'),
                    LeftRule('b', NonTerminalSymbol('B', 0u))
                )
            )
        )

        var grammarS = LeftRegularGrammar(
            NonTerminalSymbol('S', 0u),
            mapOf(
                NonTerminalSymbol('S', 0u) to setOf(
                    LeftRule('a', NonTerminalSymbol('S', 0u)),
                    LeftRule('b', NonTerminalSymbol('S', 1u))
                ),
                NonTerminalSymbol('S', 1u) to setOf(
                    LetterRule('b'),
                    LetterRule('c'),
                    LeftRule('b', NonTerminalSymbol('S', 1u))
                )
            )
        )

        assertEquals(grammarS,grammarA.renameNonTerminals('S'))

        grammarA = LeftRegularGrammar(
            NonTerminalSymbol('S', 0u),
            mapOf(
                NonTerminalSymbol('S', 0u) to setOf(
                    LeftRule('a', NonTerminalSymbol('S', 0u)),
                    LeftRule('b', NonTerminalSymbol('B', 0u)),
                    LeftRule('d', NonTerminalSymbol('D', 0u)),
                    EmptyRule
                ),
                NonTerminalSymbol('B', 0u) to setOf(
                    LetterRule('c'),
                    LetterRule('d'),
                    LeftRule('d', NonTerminalSymbol('B', 0u))
                ),
                NonTerminalSymbol('D', 0u) to setOf(
                    LeftRule('d', NonTerminalSymbol('D', 0u)),
                    EmptyRule
                )
            )
        )

        grammarS = LeftRegularGrammar(
            NonTerminalSymbol('S', 0u),
            mapOf(
                NonTerminalSymbol('S', 0u) to setOf(
                    LeftRule('a', NonTerminalSymbol('S', 0u)),
                    LeftRule('b', NonTerminalSymbol('S', 1u)),
                    LeftRule('d', NonTerminalSymbol('S', 2u)),
                    EmptyRule
                ),
                NonTerminalSymbol('S', 1u) to setOf(
                    LetterRule('c'),
                    LetterRule('d'),
                    LeftRule('d', NonTerminalSymbol('S', 1u))
                ),
                NonTerminalSymbol('S', 2u) to setOf(
                    LeftRule('d', NonTerminalSymbol('S', 2u)),
                    EmptyRule
                )
            )
        )

        assertEquals(grammarS,grammarA.renameNonTerminals('S'))
    }

    @Test
    fun concatenateTests() {
        var grammarA = LeftRegularGrammar(
            NonTerminalSymbol('S', 0u),
            mapOf(
                NonTerminalSymbol('S', 0u) to setOf(
                    LeftRule('a', NonTerminalSymbol('S', 0u)),
                    LeftRule('b', NonTerminalSymbol('B', 0u))
                ),
                NonTerminalSymbol('B', 0u) to setOf(
                    LetterRule('b'),
                    LetterRule('c'),
                    LeftRule('b', NonTerminalSymbol('B', 0u))
                )
            )
        )

        var grammarB = LeftRegularGrammar(
            NonTerminalSymbol('S', 0u),
            mapOf(
                NonTerminalSymbol('S', 0u) to setOf(
                    LeftRule('a', NonTerminalSymbol('S', 0u)),
                    LeftRule('a', NonTerminalSymbol('A', 0u))
                ),
                NonTerminalSymbol('A', 0u) to setOf(
                    LeftRule('d', NonTerminalSymbol('A', 0u)),
                    LetterRule('d')
                )
            )
        )

        var grammarAB = LeftRegularGrammar(
            NonTerminalSymbol('A', 0u),
            mapOf(
                NonTerminalSymbol('A', 0u) to setOf(
                    LeftRule('a', NonTerminalSymbol('A', 0u)),
                    LeftRule('b', NonTerminalSymbol('A', 1u))
                ),
                NonTerminalSymbol('A', 1u) to setOf(
                    LeftRule('b', NonTerminalSymbol('B', 0u)),
                    LeftRule('c', NonTerminalSymbol('B', 0u)),
                    LeftRule('b', NonTerminalSymbol('A', 1u))
                ),
                NonTerminalSymbol('B', 0u) to setOf(
                    LeftRule('a', NonTerminalSymbol('B', 0u)),
                    LeftRule('a', NonTerminalSymbol('B', 1u))
                ),
                NonTerminalSymbol('B', 1u) to setOf(
                    LeftRule('d', NonTerminalSymbol('B', 1u)),
                    LetterRule('d')
                )
            )
        )
        assertEquals(grammarAB, grammarA.concatenate(grammarB))

        /* Раскомментить как только будет реализован метод toLambdaFree
        grammarA = LeftRegularGrammar(
            NonTerminalSymbol('S', 0u),
            mapOf(
                NonTerminalSymbol('S', 0u) to setOf(
                    EmptyRule
                )
            )
        )

        grammarB = LeftRegularGrammar(
            NonTerminalSymbol('S', 0u),
            mapOf(
                NonTerminalSymbol('S', 0u) to setOf(
                    LeftRule('a', NonTerminalSymbol('S', 0u)),
                    LeftRule('b', NonTerminalSymbol('B', 0u))
                ),
                NonTerminalSymbol('B', 0u) to setOf(
                    LetterRule('c'),
                    LeftRule('b', NonTerminalSymbol('B', 0u)),
                    EmptyRule
                )
            )
        )

        grammarAB = LeftRegularGrammar(
            NonTerminalSymbol('S', 0u),
            mapOf(
                NonTerminalSymbol('S', 0u) to setOf(
                    LeftRule('a', NonTerminalSymbol('S', 0u)),
                    LeftRule('b', NonTerminalSymbol('S', 1u)),
                    EmptyRule
                ),
                NonTerminalSymbol('S', 1u) to setOf(
                    LetterRule('c'),
                    LeftRule('b', NonTerminalSymbol('S', 1u)),
                    EmptyRule
                )
            )
        )

        assertEquals(grammarAB, grammarA.concatenate(grammarB))
        */
    }
}
