package ru.appmat.toc.regular

import kotlin.test.Test
import kotlin.test.assertEquals

typealias RightRegularGrammar = RegularGrammar.RightRegularGrammar
typealias RightRule = RegularGrammarRule.NonTerminalRule.RightRule

class RightRegularGrammarTests {

    @Test
    fun unionRightGrammarWithAnotherContainingOnlyLetterRules() {
        val grammarA = RightRegularGrammar(
            NonTerminalSymbol('S', 0u),
            mapOf(
                NonTerminalSymbol('S', 0u) to setOf(
                    RightRule('a', NonTerminalSymbol('S', 0u)),
                    RightRule('b', NonTerminalSymbol('B', 0u)),
                    LetterRule('a')
                ),
                NonTerminalSymbol('B', 0u) to setOf(
                    RightRule('b', NonTerminalSymbol('B', 0u)),
                    LetterRule('b')
                )
            )
        )

        val grammarB = RightRegularGrammar(
            NonTerminalSymbol('S', 0u),
            mapOf(
                NonTerminalSymbol('S', 0u) to setOf(
                    LetterRule('a'),
                    LetterRule('b'),
                    LetterRule('c'),
                    LetterRule('d'),
                    LetterRule('e')
                )
            )
        )

        assertEquals(
            RightRegularGrammar(
                NonTerminalSymbol('S', 0u),
                mapOf(
                    NonTerminalSymbol('S', 0u) to setOf(
                        RightRule('a', NonTerminalSymbol('A', 0u)),
                        RightRule('b', NonTerminalSymbol('A', 1u)),
                        LetterRule('a'),
                        LetterRule('b'),
                        LetterRule('c'),
                        LetterRule('d'),
                        LetterRule('e')
                    ),
                    NonTerminalSymbol('A', 0u) to setOf(
                        RightRule('a', NonTerminalSymbol('A', 0u)),
                        RightRule('b', NonTerminalSymbol('A', 1u)),
                        LetterRule('a')
                    ),
                    NonTerminalSymbol('A', 1u) to setOf(
                        RightRule('b', NonTerminalSymbol('A', 1u)),
                        LetterRule('b')
                    ),
                    NonTerminalSymbol('B', 0u) to setOf(
                        LetterRule('a'),
                        LetterRule('b'),
                        LetterRule('c'),
                        LetterRule('d'),
                        LetterRule('e')
                    )
                )
            ),
            grammarA union grammarB
        )
    }

    @Test
    fun unionRightGrammarWithAnother() {
        val grammarA = RightRegularGrammar(
            NonTerminalSymbol('S', 0u),
            mapOf(
                NonTerminalSymbol('S', 0u) to setOf(
                    RightRule('a', NonTerminalSymbol('S', 0u)),
                    RightRule('b', NonTerminalSymbol('B', 0u)),
                    LetterRule('a')
                ),
                NonTerminalSymbol('B', 0u) to setOf(
                    RightRule('b', NonTerminalSymbol('B', 0u)),
                    LetterRule('b')
                )
            )
        )

        val grammarB = RightRegularGrammar(
            NonTerminalSymbol('S', 0u),
            mapOf(
                NonTerminalSymbol('S', 0u) to setOf(
                    RightRule('c', NonTerminalSymbol('S', 0u)),
                    RightRule('d', NonTerminalSymbol('D', 0u)),
                    LetterRule('c')
                ),
                NonTerminalSymbol('D', 0u) to setOf(
                    RightRule('d', NonTerminalSymbol('D', 0u)),
                    LetterRule('d')
                )
            )
        )

        assertEquals(
            RightRegularGrammar(
                NonTerminalSymbol('S', 0u),
                mapOf(
                    NonTerminalSymbol('S', 0u) to setOf(
                        RightRule('a', NonTerminalSymbol('A', 0u)),
                        RightRule('b', NonTerminalSymbol('A', 1u)),
                        RightRule('c', NonTerminalSymbol('B', 0u)),
                        RightRule('d', NonTerminalSymbol('B', 1u)),
                        LetterRule('a'),
                        LetterRule('c')
                    ),
                    NonTerminalSymbol('A', 0u) to setOf(
                        RightRule('a', NonTerminalSymbol('A', 0u)),
                        RightRule('b', NonTerminalSymbol('A', 1u)),
                        LetterRule('a')
                    ),
                    NonTerminalSymbol('A', 1u) to setOf(
                        RightRule('b', NonTerminalSymbol('A', 1u)),
                        LetterRule('b')
                    ),
                    NonTerminalSymbol('B', 0u) to setOf(
                        RightRule('c', NonTerminalSymbol('B', 0u)),
                        RightRule('d', NonTerminalSymbol('B', 1u)),
                        LetterRule('c')
                    ),
                    NonTerminalSymbol('B', 1u) to setOf(
                        RightRule('d', NonTerminalSymbol('B', 1u)),
                        LetterRule('d')
                    )
                )
            ),
            grammarA union grammarB
        )
    }

    @Test
    fun unionRightGrammarWithItself() {
        val b = NonTerminalSymbol('B')
        val a = NonTerminalSymbol('A')

        val rightRegularGrammar = RightRegularGrammar(
            a,
            mapOf(
                a to setOf(
                    RightRule('b', b),
                    LetterRule('c')
                ),
                b to setOf(
                    RightRule('a', a)
                )
            )
        )
        val s = NonTerminalSymbol('S')

        assertEquals(
            RightRegularGrammar(
                s,
                mapOf(
                    s to setOf(
                        RightRule('b', a.copy(index = 1u)),
                        RightRule('b', b.copy(index = 1u)),
                        LetterRule('c')
                    ),
                    a to setOf(
                        RightRule('b', a.copy(index = 1u)),
                        LetterRule('c')
                    ),
                    b to setOf(
                        RightRule('b', b.copy(index = 1u)),
                        LetterRule('c')
                    ),
                    a.copy(index = 1u) to setOf(
                        RightRule('a', a)
                    ),
                    b.copy(index = 1u) to setOf(
                        RightRule('a', b)
                    )
                )
            ),
            rightRegularGrammar union rightRegularGrammar
        )
    }
}
